#!/usr/bin/env python
import sys
#import pdb; pdb.set_trace()
#cat student_analytics_fake_data.json | python2 mapper.py | sort -k1,1 | python2 reducer.py 
'''CORE COURSES'''
#['MATH 162: Calculus I','PHYC 160: General Physics I', 
#'MATH 163: Calculus II', 'PHYC 161: General Physics II', 'ECE 203: Circuit Analysis I', 
#'ECE 213: Circuit Analysis II', 'MATH 327: Discrete Structures']

core_c=['MATH 162','PHYC 160', 
'MATH 163', 'PHYC 161', 'ECE 203', 
'ECE 213', 'MATH 327']

num_cc=0       #number of students with core courses
num_students=0
num_c=0
#pdb.set_trace()
for line in sys.stdin:
    #courses=line[9:]  #[Courses::[ECE 101] 
    courses=line
    num_students+=1
    num_c+=line.count(',')+1 #["PHYC 160, PHYC 160L","ENGL 120","MATH 162"]
    #print courses
    #look for a core course
    for cc in core_c:
        if cc in courses:
            num_cc+=1
            break
# avg courses taken by student
# #courses/students
print"Avg # of courses taken by student",float(num_c)/num_students
print"Number of students with a core course",num_cc
            

