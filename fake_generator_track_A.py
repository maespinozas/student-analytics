#!/usr/bin/env python2
import sys, os, platform, glob
import csv
import random

def getdata(filename1):
    file=open(filename1, "r")
    lines=file.readlines()
    file.close()
    degreeplan=[[], [], []]     #degree name, courses, possible grades
    degreecourses=[]
    degree="something"  #initialization
    grades=['A','A-','B+','B','B-', 'C+','C','C-'] #initialization
    #ex. [computer eng,[list of courses]]
    c=0
    for x in lines:
        data=(x.split(","))   #add csv data
        c+=1
        if (c>1):   #after headers
            if(degree!=data[0] and len(degreecourses)>1):   #to check when to write back the courses list in the array
                degreeplan[0].append(degree)                    #name of degree
                degreeplan[1].append(degreecourses)       #courses for that degree
                degreecourses=[]                                        #cleaning the list
            degree=data[0]
            #degreecourses.append(data[2]) #course
            degreecourses.append(cleanCourseString(data[2])) 
            if(data[4] not in grades):       #to add specific grades like C- ,etc
                #if(data[4]=='3'):
                #    print("hello cindy")
                grades.append(data[4])
    for grade in grades:
        degreeplan[2].append(grade)
    return degreeplan                               #degreeplan[0] list of degrees degreeplan[1] list of courses

def createfake(num_students, degreeplan):
    c=0
    for plan in degreeplan[0]:
        print (str(c)+" "+str(plan))
        c+=1
    plan=input("Select a degree:\n")
    print("you chose degree (number):"+str(degreeplan[0][plan]))
    courses=degreeplan[1][plan]
    print "## List of Courses for the degree ##"
    print courses
    print "#"*10
    #format needed
    #{id: "", semesters_attended: 1, courses_taken: [{name: "MATH 162", grade: "C"}]}
    out=[]
    for x in range(num_students):
        id=x
        semesters=random.randint(1, 7)
        ptrack=random.random()
        track=False
        if (ptrack>.5):
            track=True
        num_of_courses=random.randint(1, len(courses))
        courses_string="courses_taken: ["
        first=True
        for coursetaken in range(num_of_courses):
            #grade=degreeplan[2][random.randint(0, len(degreeplan[2])-1)]
	    grade="A"
            if(first==True):
                courses_string+="{name:"+'"'+str(courses[random.randint(0, len(courses)-1)])+'", grade:'+'"'+str(grade)+'"'+'}'
                first=False
            else:
                courses_string+=",{name:"+'"'+str(courses[random.randint(0, len(courses)-1)])+'", grade:'+'"'+str(grade)+'"'+'}'
        courses_string+="],track:"+'"'+str(track)+'"'+",ptrack:"+str(ptrack)+"}"
        out.append('{id:'+'"'+str(id)+'", semesters_attended: '+str(semesters)+","\
        +courses_string)
    return out
        
def createJSON(data):
    file=open((os.path.dirname(os.path.realpath(__file__))+'/'+'fakedata_A'+'.json'),mode='w')
    for student in data:
        file.write(student+"\n")
    file.close()
    print("File fakedata.json saved")

def cleanCourseString(course):
    cleancourse=""
    for char in course:
        if ((char.isupper())or (char.isdigit()) or (char==" ")): #ex. ECE 101
            cleancourse+=char
        elif(char==":"):
            break
    if (cleancourse[1].isupper==False or cleancourse[1]==" "): #ex. Seminar
        cleancourse=course
        
    return cleancourse

#ex. degreeplan[0][1] = computer eng
#theb degreeplan[1][1]= list of courses for comp eng
#this is the excpected format for each student

'''{
    "id": "_9485hc6563hj",
    "semesters_attended": 1,
    "courses_taken": [
        {
            "name": "MATH 162",
            "grade": "C"
        },
        {
            "name": "PHYC 160",
            "grade": "B"
        },
        {
            "name": "ECE 101",
            "grade": "A"
        },
        {
            "name": "ECE 131",
            "grade": "A"
        }
    ]
}
'''


#filename1=str(raw_input("Insert csv file path:"))      #uncomment to manually specify the file
filename1="/home/mrhyde/ownCloud/ece/Web Architecture/hw/students-project/code/degree-plans.csv"
degreeplan=getdata(filename1)
students=input("How many students??\n")
out=createfake(students, degreeplan)
createJSON(out)

