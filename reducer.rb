#!/usr/bin/env ruby
=begin
to test outside hadoop cat fakedata.json | ruby mapperv2.rb | sort -k1,1 |ruby reducerv2.rb
GPA CONV. TABLE
A+ 97-100  4.0
A 93-96    4.0
A-  90-92   3.7
B+  87-89   3.3
B   83-89   3.0
B-  80-82   2.7
C+  77-79   2.3
C   73-76   2.0
C-  70-72   1.7
D+  67-69   1.3
D   65-66   1.0

metric id metric
1       %track
2       Av GPA overall
3       Av GPA per term
4       Av GPA per Course
=end

metric_flag=true
nlines=0
$metricid=0
$n_st=0
$n_st_ontrack=0
$pontrack=0
$grades_dict = Hash.new(0)  #to have default value 0 and not nil
$grades_dict={"A+"=>4.0,"A"=>4.0 \
,"A"=>3.7,"B+"=>3.3,"B"=>3.0, \
"B-"=>2.7,"C+"=>2.3 \
,"C"=>2.0,"C-"=>1.7 \
,"D"=>1.3,"D-"=>1.0}
$term_gpa = Hash.new(0)  #to have default value 0 and not nil
$term_gpa={"1"=>0.0,"n_st_1"=>0.0,"2"=>0.0,"n_st_2"=>0.0 \
,"3"=>0.0,"n_st_3"=>0.0,"4"=>0.0,"n_st_4"=>0.0,"5"=>0.0,"n_st_5"=>0.0 \
,"6"=>0.0,"n_st_6"=>0.0,"7"=>0.0,"n_st_7"=>0.0}  #n_st_7 number of students in term 7
$gpa=0.0
$n_students=0.0
$prev_course=""   #to be used in metric 4


def metric(mid,line)
  case mid
  when 1    #GETTING ex.  T,0.7     (ontrack??,track percentage)
    is_on_track=line[0] #getting T
    if(is_on_track=='T')
      $n_st_ontrack+=1  #one more on track
    end
    ptrack=line[line.index(',')+1...line.size()-1]   #getting 0.7
    $pontrack+=ptrack.to_f
  when 2,3  #GETTING A,B,A,C-, etc
    stop=false
    n_courses=1.0
    gpast=0.0   #gpa single student
    n_students_in_term=0.0  #Used fot metric 3
    if(mid==3)
      term=line[line.index("T")+1...line.index(',')]
      line=line[line.index(',')+1...line.size()]    #shift line
      tmp_string="n_st_"+term.to_s
    end
    while(stop==false)    #do this for each line
      if(line.index(',')==nil)
        grade=line
      else
        grade=line[0...line.index(',')]
        line=line[line.index(',')+1...line.size()]
      end
      gpast+=$grades_dict[grade].to_f
      n_courses+=1
      #$n_students+=1    #used for global GPA
      #print(grade)
      if(line.index(',')==nil)
        grade=line        #get last grade
        gpast=gpast/n_courses.to_f    #GPA for a single student
        #puts("GPA:"+gpast.to_s)
        $gpa+=gpast                   #accumulative gpa for the cohort
        if(mid==3)
          $term_gpa[tmp_string]+=1  #add one student to the term
          $term_gpa[term.to_s]+=gpast    #accumulate gpas for all students in that term
        end
        stop=true
        break
      end
    end
      #$gpa+=$grades_dict[]
  when 4  #Av GPA per course
    course=line[0...line.index(',')]
    grade=line[line.index(',')+1...line.index("\n")]
    #puts(grade)
    #puts($grades_dict[grade])
    if (course!=$prev_course)
      if($prev_course.size()>1)
        $gpa+=$grades_dict[grade].to_i#new course, print GPA information about this one
        $n_students+=1.0
        $gpa=$gpa/$n_students
        puts($prev_course.to_s+":"+$gpa.to_s)
        $gpa=0.0            #reset for next course
        $n_students=0.0
      end
      $prev_course=course         #new course
    else                          #same course again, add the GPA
      $n_students+=1.0
      $gpa+=$grades_dict[grade].to_f
    end
  when String
    puts("Fixnum")
  else
    puts("else case")
    ##something
  end
end

STDIN.each_line do |line|
  if(metric_flag==true)
    $metricid=line.to_i    #convert to integer
    metric_flag=false   #to do this only for the first line
  else
    #puts($metricid)
    metric($metricid,line)
    $n_st+=1.0           #number of lines=number of students
    #puts($n_st)
  end
end


### After processing all the lines
case $metricid
when 1    #%TRACK
  track=($n_st_ontrack/$n_st.to_f)*100#get the overall %track   studontrack/total * 100
  ptrack=($pontrack/$n_st.to_f)   #the overall coefficient of on track
  puts("students on track:"+track.to_s+"%")
  puts("Av track (progress) of the students:"+ptrack.to_s)
when 2 #Av GPA for the cohort
  puts("Av GPA for the cohort:"+($gpa/$n_st).to_s)
when 3 #Av GPA per term
  t=1
  tmp_string="n_st_"
  while(t<8)
    #puts("accumulative gpa:"+$term_gpa[t.to_s].to_s+","+"# of studenst in the term:"+$term_gpa[tmp_string+t.to_s].to_s)
    $term_gpa[t.to_s]=$term_gpa[t.to_s]/$term_gpa[tmp_string+t.to_s] #calculate GPA for each term and display
    puts("TERM "+t.to_s+" GPA:"+$term_gpa[t.to_s].to_s)
    t+=1
  end
else
  #
end
