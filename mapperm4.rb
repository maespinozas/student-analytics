#!/usr/bin/env ruby
=begin
STUDENT ANALYTICS MAPPER

metric id metric
1       %track
2       Av GPA overall
3       Av GPA per term
4       Av GPA per Course
5       Students and GPA
6       Course GPA and number of students
=end

$metricid=4
$n_st=0
$n_st_ontrack=0
$pontrack=0

def metric(mid,line)
  case mid
  when 4  #AV GPA per course   , ex. ECE 251,A
    while(1)
      start=line.index("name:")+6
      line=line[start...line.size()]
      course=line[0...line.index(",")-1]  #"ECE 340",
      #puts(course)
      gradestart=line.index('grade:"')+7
      line=line[gradestart...line.size()]
      grade=line[0...line.index('"')]
      #puts(grade)
      line=line[line.index('"')+1...line.size()]
      puts(course.to_s+","+grade.to_s)
      break if(line[1]=="]") #ex. grade:"B-"}]  , detect end of grades
    end
  else
    puts("hello cindy")
    ##something
  end
end

## MAIN ###
metric_flag=true
STDIN.each_line do |line|
  metric($metricid,line)
end
if($metricid==4)
  puts("zzzendoffile,a") #cacth this as end of stream, remember hadoop orders by key
end
