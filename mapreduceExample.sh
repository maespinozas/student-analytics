#!/bin/bash
# declare STRING variable
STRING="Run your mapreduce job"
#print variable on a screen
echo $STRING
echo -e "Type name of output dir: \c "
read outdir
echo "You selected dir: $outdir"
echo -e "Mapper file: \c "
read mapper
echo -e "Reducer file: \c "
read reducer

/usr/local/hadoop/bin/hadoop \
jar /usr/local/hadoop/share/hadoop/tools/lib/hadoop-streaming-2.5.1.jar \
-files $mapper,$reducer \
-mapper $mapper \
-reducer $reducer \
-input /input/fakedata.json \
-output $outdir \
&& /usr/local/hadoop/bin/hadoop fs -get $outdir/part* /home/hduser/out.txt

#bin/hadoop jar share/hadoop/tools/lib/hadoop-streaming-2.5.1.jar -files /home/hduser/mapperm1.rb,/home/hduser/reducerm1.rb -mapper /home/hduser/mapperm1.rb -reducer /home/hduser/reducerm1.rb -input /input/fakedata.json -output /outM1_22 && bin/hadoop fs -get /outM1_22/* /home/hduser/


