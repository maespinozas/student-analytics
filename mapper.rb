#!/usr/bin/env ruby
=begin
STUDENT ANALYTICS REDUCER

metric id metric
1       %track
2       Av GPA overall
3       Av GPA per term
4       Av GPA per Course

first line should be something like this:
metric,4

=end

$metricid=0
$n_st=0
$n_st_ontrack=0
$pontrack=0

def metric(mid,line)
  case mid
  when 1  ## for each student print: ontrack,pontrack , ex. T,0.35
    is_on_track=line[line.index('track:')+7]#get boolean track get T or F form True and False, track:"True"
    ptrack=line[line.index('ptrack:')+7...line.size()-2]   #ex. ptrack:0.560254942972}
    print(is_on_track+","+ptrack+"\n")
  when 2,3  ## for each student the list of grades, ex. A,A-,B,C,C+
    stop=""
    grade=""
    termindex=line.index("semesters_attended:")+"semesters_attended:".size+1  #ex. semesters_attended: 6
    term=line[termindex]
    while(stop!=nil)
        #get the grade: position
        break if(line[2]=="]") #ex. grade:"B-"}],track:"False",ptrack:0.3
        start=line.index("grade:")+6+1    #grade: 6
        #puts(start)
        if(start==nil)
          stop=start
          break
        end
        endindex=line[start...line.size()].index('"') #get the end of grade ex. C"
        #puts(endindex)
        if(grade.size()==0)
          grade+=line[start...start+endindex]
        else
          grade+=","+line[start...start+endindex]
        end
        line=line[start+endindex...line.size()]  #to get next grade start from where last grade ended
    end
    if mid==3
      print("T"+term.to_s+","+grade+"\n")     #ex. T3,A,A-,B,C,C+
    else
      puts(grade)                         #ex. A,A-,B,C,C+
    end
  when 4  #AV GPA per course   , ex. ECE 251,A
    while(1)
      start=line.index("name:")+6
      line=line[start...line.size()]
      course=line[0...line.index(",")-1]  #"ECE 340",
      #puts(course)
      gradestart=line.index('grade:"')+7
      line=line[gradestart...line.size()]
      grade=line[0...line.index('"')]
      #puts(grade)
      line=line[line.index('"')+1...line.size()]
      puts(course.to_s+","+grade.to_s)
      break if(line[1]=="]") #ex. grade:"B-"}]  , detect end of grades
    end
  else
    puts("else case")
    ##something
  end
end

## MAIN ###
metric_flag=true
id=0
STDIN.each_line do |line|
  if (metric_flag==true)
    puts(line[line.index(',')+1...line.size()-2])   #remove metric, ex. {metric,2}
    metric_flag=false   #to do this only for the first line
    id=line[line.index(',')+1...line.size()].to_i   #value of the metric
  else
    metric(id,line)
  end
end
if(id==4)
  puts("zzzendoffile,a") #cacth this as end of stream, remember hadoop orders by key
end
