#!/usr/bin/env ruby
=begin
to test outside hadoop cat fakedata.json | ruby mapperv2.rb | sort -k1,1 |ruby reducerv2.rb
GPA CONV. TABLE
A+ 97-100  4.0
A 93-96    4.0
A-  90-92   3.7
B+  87-89   3.3
B   83-89   3.0
B-  80-82   2.7
C+  77-79   2.3
C   73-76   2.0
C-  70-72   1.7
D+  67-69   1.3
D   65-66   1.0

metric id metric
1       %track
2       Av GPA overall
3       Av GPA per term
4       Av GPA per Course
=end

metric_flag=true
nlines=0
$n_st=0
$n_st_ontrack=0
$pontrack=0
$grades_dict = Hash.new(0)  #to have default value 0 and not nil
$grades_dict={"A+"=>4.0,"A"=>4.0 \
,"A"=>3.7,"B+"=>3.3,"B"=>3.0, \
"B-"=>2.7,"C+"=>2.3 \
,"C"=>2.0,"C-"=>1.7 \
,"D"=>1.3,"D-"=>1.0}
$term_gpa = Hash.new(0)  #to have default value 0 and not nil
$term_gpa={"1"=>0.0,"n_st_1"=>0.0,"2"=>0.0,"n_st_2"=>0.0 \
,"3"=>0.0,"n_st_3"=>0.0,"4"=>0.0,"n_st_4"=>0.0,"5"=>0.0,"n_st_5"=>0.0 \
,"6"=>0.0,"n_st_6"=>0.0,"7"=>0.0,"n_st_7"=>0.0}  #n_st_7 number of students in term 7
$gpa=0.0
$n_students=0.0
$prev_course=""   #to be used in metric 4


def metric(mid,line)
  case mid
  when 1    #GETTING ex.  T,0.7     (ontrack??,track percentage)
    is_on_track=line[0] #getting T
    if(is_on_track=='T')
      $n_st_ontrack+=1  #one more on track
    end
    ptrack=line[line.index(',')+1...line.size()-1]   #getting 0.7
    $pontrack+=ptrack.to_f
  else
    puts("else case")
    ##something
  end
end

STDIN.each_line do |line|
  metric(1,line)
  $n_st+=1.0           #number of lines=number of students
  #puts($n_st)
end


### After processing all the lines
track=($n_st_ontrack/$n_st.to_f)*100#get the overall %track   studontrack/total * 100
ptrack=($pontrack/$n_st.to_f)   #the overall coefficient of on track
puts("Students on track:"+track.round(2).to_s+"%")
puts("Av track (progress) of the students:"+ptrack.round(2).to_s)
