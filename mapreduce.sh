#!/bin/bash

#print variable on a screen

METRIC=$1
DESTFOLDER=$2
outdir=/out
mapper=~/mapperm$METRIC.rb
reducer=~/reducerm$METRIC.rb

echo "Mapper:$mapper"
echo "Reducer:$reducer"
echo "DestFolder:$DESTFOLDER"

if [ ! -f $DESTFOLDER/out.txt ]; then
    echo "File out.txt not found!"
else
    rm $DESTFOLDER/out.txt
    echo "deleted file out.txt"
fi


#delete /out directory
#/usr/local/hadoop/bin/hadoop fs -rm -R /out* && \
#run job
/usr/local/hadoop/bin/hadoop \
jar /usr/local/hadoop/share/hadoop/tools/lib/hadoop-streaming-2.2.0.jar \
-files $mapper,$reducer \
-mapper $mapper \
-reducer $reducer \
-input /input/fakedata.json \
-output $outdir \
&& /usr/local/hadoop/bin/hadoop fs -get $outdir/part* $DESTFOLDER/out.txt \
&& /usr/local/hadoop/bin/hadoop fs -rm -R $outdir* \
&& cat $DESTFOLDER/out.txt | sort -k 1 >> $DESTFOLDER/out.txt \
&& cat $DESTFOLDER/out.txt
